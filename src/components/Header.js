import React from "react";
import { NavLink } from "react-router-dom";

import { FirebaseContext } from '../firebase';

function Header() {

  const { user, firebase } = React.useContext(FirebaseContext);

  return (
    <header>
      <nav className="navbar navbar-expand-lg navbar-light bg-light d-flex justify-content-between align-items-center">
        <NavLink to="/">Hooks News</NavLink>
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink className="nav-link" to="/top">Top</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/search">Search</NavLink>
          </li>
          {user && (
            <li className="nav-item">
              <NavLink className="nav-link" to="/create">Submit</NavLink>
            </li>
          )}

        </ul>
        <ul className="navbar-nav">
          {user
            ? (
              <>
                <li className="nav-item d-flex">
                  <span className="d-block" style={{ padding: "0.5rem 1rem" }}> {user.displayName} </span>
                  <button type="button" onClick={() => firebase.logout()} className="btn btn-danger">Logout</button>
                </li>
              </>) : (<li className="nav-item">
                <NavLink className="nav-link" to="/login">Login</NavLink>
              </li>)}
        </ul>
      </nav>
    </header>
  )
}

export default Header;
