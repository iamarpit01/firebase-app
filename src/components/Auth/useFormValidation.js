import React from "react";

function useFormValidation(initialState, validateLogin, authenticate) {
    const [values, setValues] = React.useState(initialState);
    const [errors, setErrors] = React.useState({});
    const [isSubmitting, setIsSubmitting] = React.useState(false);

    React.useEffect(() => {
        if (isSubmitting) {
            const noErrors = Object.keys(errors).length === 0;
            if (noErrors) {
                authenticate();
                setIsSubmitting(false);
            }
            else {
                setIsSubmitting(false);
            }
        }
    }, [errors])

    function onHandleChange(evt) {
        evt.persist();
        setValues(previousValues => ({
            ...previousValues,
            [evt.target.name]: evt.target.value
        }))
    }

    function onHandleBlur() {
        const validationErrors = validateLogin(values);
        setErrors(validationErrors);
    }

    function onHandleSubmit(evt) {
        evt.preventDefault();
        const validationErrors = validateLogin(values);
        setErrors(validationErrors);
        setIsSubmitting(true);
    }

    return {
        onHandleBlur,
        onHandleSubmit,
        onHandleChange,
        values,
        errors,
        isSubmitting
    }
}

export default useFormValidation;
