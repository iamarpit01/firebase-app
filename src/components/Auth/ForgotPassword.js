import React from "react";
import { FirebaseContext } from "../../firebase";

function ForgotPassword() {
  const { firebase } = React.useContext(FirebaseContext);
  const [passwordResetEmail, setPasswordResetEmail] = React.useState('');
  const [isPasswordReset, setIsPasswordReset] = React.useState(false);
  const [passwordResetErr, setPasswordResetErr] = React.useState(null);

  async function handleResetPassword() {
    console.log("calling");
    try {
      await firebase.resetPassword(passwordResetEmail);
      setIsPasswordReset(true);
    }
    catch (err) {
      setPasswordResetErr(err.message);
      setIsPasswordReset(false);
    }
  }

  return (
    <div>
      <input
        type="email"
        placeholder="Enter Email"
        onChange={(evt) => setPasswordResetEmail(evt.target.value)} />

      <div>
        <button
          type="button"
          className="btn btn-primary"
          onClick={() => handleResetPassword()}>Submit</button>
      </div>
      <div>
        {isPasswordReset && <span>Check your email id to reset password.</span>}
        {passwordResetErr && <span className="text-danger">{passwordResetErr}</span>}
      </div>
    </div>
  );
}

export default ForgotPassword;
