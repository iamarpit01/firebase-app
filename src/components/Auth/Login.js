import React, { useState } from "react";
import useFormValidation from './useFormValidation';
import validateLogin from './validateLogin';
import firebase from '../../firebase';
import { NavLink } from "react-router-dom";

const loginStyle = {
  width: "300px"
}

const INITIAL_STATE = {
  name: "",
  email: "",
  password: ""
}

function Login(props) {

  const { onHandleBlur, onHandleSubmit, onHandleChange, values, errors, isSubmitting } = useFormValidation(INITIAL_STATE, validateLogin, authenticateUser);
  const [login, setLogin] = useState(true);
  const [firebaseError, setFirebaseError] = useState(null);

  async function authenticateUser() {
    const { name, email, password } = values;
    try {
      const response = login
        ? await firebase.login(email, password)
        : await firebase.register(name, email, password);

      props.history.push("/")
    }
    catch (err) {
      setFirebaseError(err.message);
      console.log(err.message)
    }
  }

  return (
    <div>
      <form onSubmit={onHandleSubmit}>
        <div className="d-flex justify-content-center">
          <div className="card" style={loginStyle}>
            <div className="card-body">
              <h3>{login ? "Login" : "Create An Account"}</h3>
              {!login && <div className="form-group">
                <label htmlFor="exampleInputName">Your Name</label>
                <input type="text" name="name" value={values.name}
                  id="exampleInputName"
                  onChange={onHandleChange} />
              </div>
              }

              <div className="form-group">
                <label htmlFor="exampleInputEmail">Email address</label>
                <input type="email" name="email" value={values.email}
                  className={`form-control ${errors.email && "error-input"}`}
                  id="exampleInputEmail"
                  onChange={onHandleChange}
                  onBlur={onHandleBlur} />

                {errors.email && <p className="text-danger font-weight-normal"><small>{errors.email}</small></p>}
              </div>

              <div className="form-group">
                <label htmlFor="exampleInputPassword">Password</label>
                <input type="password" name="password" value={values.password}
                  className={`form-control ${errors.password && "error-input"}`}
                  id="exampleInputPassword"
                  onChange={onHandleChange}
                  onBlur={onHandleBlur} />

                {errors.password && <p className="text-danger font-weight-normal"><small>{errors.password}</small></p>}
                {firebaseError && <p className="text-danger font-weight-normal"><small>{firebaseError}</small></p>}
              </div>

              <div className="form-group d-flex justify-content-between">
                <button type="submit" className="btn btn-primary">Submit</button>
                <button type="button" className="btn btn-link" onClick={() => setLogin(prevlogin => !prevlogin)}>
                  {login ? "Create An Account" : "Login Here"}
                </button>
              </div>

              <div>
                <NavLink to="/forgot" className="btn btn-link p-0">Forgot Password</NavLink>
              </div>

            </div>
          </div>
        </div>
      </form>
    </div>
  );
}

export default Login;
